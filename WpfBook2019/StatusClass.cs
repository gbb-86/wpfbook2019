﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace WpfBook2019
{
    class StatusClass : INotifyPropertyChanged
    {
        #region network
        private bool networkStatusOkField;
        public bool networkStatusOkProperty {
            get
            {
                return this.networkStatusOkField;
            }
            set
            {
                if (value != this.networkStatusOkField)
                {
                    this.networkStatusOkField = value;
                    NotifyPropertyChanged("networkStatusOkProperty");
                }
            }
        }
        private bool checkNetworkStatus()
        {
            var task = Task.Run(() => checkReachableAdress("http://google.com/generate_204"));
            //return checkReachableAdress("http://google.com/generate_204");
            return true;
        }
        #endregion

        #region youtubedl
        private bool youtubedlFoundField;
        public bool youtubedlFoundProperty
        {
            get
            {
                return this.youtubedlFoundField;
            }
            set
            {
                if (value != this.youtubedlFoundField)
                {
                    this.youtubedlFoundField = value;
                    NotifyPropertyChanged("youtubedlFoundProperty");
                }
            }
        }
        private bool checkYoutubedlStatus()
        {
            return File.Exists(ConfigurationManager.AppSettings["youtubedlPath"].ToString());
        }
        #endregion

        #region ffmpeg
        private bool ffmpegFoundField;
        public bool ffmpegFoundProperty
        {
            get
            {
                return this.ffmpegFoundField;
            }
            set
            {
                if (value != this.ffmpegFoundField)
                {
                    this.ffmpegFoundField = value;
                    NotifyPropertyChanged("ffmpegFoundProperty");
                }
            }
        }
        private bool checkFfmpegFoundStatus()
        {
            bool ffmpeg = File.Exists(ConfigurationManager.AppSettings["ffmpeg"].ToString());
            bool ffplay = File.Exists(ConfigurationManager.AppSettings["ffplay"].ToString());
            bool ffprobe = File.Exists(ConfigurationManager.AppSettings["ffprobe"].ToString());
            return ffmpeg && ffplay && ffprobe;
        }
        #endregion

        #region youtubeLink
        private bool youtubeLinkField;
        public bool youtubeLinkProperty
        {
            get
            {
                return this.youtubeLinkField;
            }
            set
            {
                if (value != this.youtubeLinkField)
                {
                    this.youtubeLinkField = value;
                    NotifyPropertyChanged("youtubeLinkProperty");
                }
            }
        }
        public async void checkYoutubeLinkStatus(string testo)
        {
            Uri uriResult;
            bool uriCheck = Uri.TryCreate(testo, UriKind.Absolute, out uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            //bool uriReach = await checkReachableAdress(testo);
            bool uriReach = await IsConnected(testo);
            youtubeLinkProperty = uriCheck && uriReach;
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public StatusClass()
        {
            networkStatusOkProperty = checkNetworkStatus();
            youtubedlFoundProperty = checkYoutubedlStatus();
            ffmpegFoundProperty = checkFfmpegFoundStatus();
        }

        private async Task<bool> checkReachableAdress(string address)
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead(address))
                    return true;
            }
            catch
            {
                return false;
            }
        }

        private async Task<bool> IsConnected(string address)
        {
            int port = 80;

            try
            {
                using (TcpClient client = new TcpClient())
                {
                    var clientTask = client.ConnectAsync(address, port);
                    var delayTask = Task.Delay(2000);

                    var completedTask = await Task.WhenAny(new[] { clientTask, delayTask });

                    return completedTask == clientTask;
                }
            }
            catch (Exception exception)
            {
                return false;
            }
        }
    }
}
