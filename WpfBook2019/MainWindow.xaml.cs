﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfBook2019
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private StatusClass statusClass = new StatusClass();
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = statusClass;
        }

        private void incollaAppuntiButton_Click(object sender, RoutedEventArgs e)
        {
            IDataObject iData = Clipboard.GetDataObject();
            if (iData.GetDataPresent(DataFormats.Text)) 
            {
                linkTextBox.Text = (String)iData.GetData(DataFormats.Text);
            }
        }

        private void linkTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            statusClass.checkYoutubeLinkStatus(linkTextBox.Text);
        }
    }
}
